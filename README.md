# Computer Basics

This three part series on the basics of computer hardware, software and programming is freely available, and is written for those who have limited or no knowledge of personal computing.

Part 1: `The Basics of Computing (hardware).pdf`

Part 2: `The Basics of Computing (software).pdf`

Part 3: `The Basics of Computing (programming).pdf`
